# STEP PROJECT FORKIO 
## Step project FORKIO by Balashov Jevgen

### A project written by such a technology as GULP

### List of plugins used:
1. browser-sync
2. del
3. gulp-autoprefixer
4. gulp-clean-css
5. gulp-file-include
6. gulp-fonter-fix
7. gulp-group-css-media-queries
8. gulp-if
9. gulp-imagemin
10. gulp-newer
11. gulp-notify
12. gulp-plumber
13. gulp-rename
14. gulp-replace
15. gulp-sass
16. gulp-sourcemaps
17. gulp-ttf2woff2
18. gulp-version-number
19. gulp-babel
20. @babel/core
21. @babel/preset-env
22. gulp-concat
23. gulp-uglify
24. sass

###### Project written only by Jevgen Balashov